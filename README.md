My automated APT repo
=====================
[![pipeline status](https://gitlab.com/psliwka/apt.psliwka.app/badges/master/pipeline.svg)](https://gitlab.com/psliwka/apt.psliwka.app/commits/master)

This is an APT repository which pulls stuff I like from various places in the internet,
and makes it easily installable/upgradeable on any Debian-ish machine.

The repository is autoupdated daily,
using scheduled [Gitlab CI](https://docs.gitlab.com/ce/ci/) pipelines.

Some packages are simply downloaded as debs from upstream
(be it Github releases pages, or other APT repositories),
some are built within this project's pipelines.

Provided packages
-----------------

* [terraform](https://www.terraform.io/)
* [vault](https://www.vaultproject.io/)
* [miniflux](https://miniflux.app/)
* [mint-themes](https://github.com/linuxmint/mint-themes)
* [mint-x-icons](https://github.com/linuxmint/mint-x-icons)
* [mint-y-icons](https://github.com/linuxmint/mint-y-icons)

Usage
-----

On Debian Stretch and earlier,
it is recommended to install `apt-transport-https` package first:
```sh
$ sudo apt-get update && sudo apt-get install apt-transport-https
```

Import the repo signing key:
```sh
$ wget -q -O - https://apt.psliwka.app/signing.key | sudo apt-key add -
```

Then add the repo to sources list:
```sh
$ echo "deb https://apt.psliwka.app unstable main" | sudo tee /etc/apt/sources.list.d/psliwka.list
```

After next `apt update`,
new packages should be available to install.
