#!/usr/bin/env python3

import os

from github import Github
import wget


service = Github(os.environ.get('GITHUB_TOKEN'))
repo = service.get_repo('miniflux/miniflux')
newest_release = repo.get_releases()[0]
for asset in newest_release.get_assets():
    if asset.name.endswith('.deb'):
        print(f"downloading {asset.browser_download_url}")
        filename = wget.download(asset.browser_download_url, out="debs/")
        print(f"\ndownloaded to {filename}")
