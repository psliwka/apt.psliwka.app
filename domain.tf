data "cloudflare_zones" "psliwka_app" {
  filter {
    name = "psliwka.app"
  }
}

resource "cloudflare_record" "main_domain" {
  zone_id = "${data.cloudflare_zones.psliwka_app.zones[0].id}"
  name    = "apt"
  type    = "CNAME"
  value   = "psliwka.gitlab.io"
  proxied = false
}

resource "cloudflare_record" "gitlab_pages_verification" {
  zone_id = "${data.cloudflare_zones.psliwka_app.zones[0].id}"
  name    = "_gitlab-pages-verification-code.apt"
  type    = "TXT"
  value   = "gitlab-pages-verification-code=a67962c2e1c6a5b7055bb933b969652a"
  proxied = false
}
