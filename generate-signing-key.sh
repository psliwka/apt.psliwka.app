#!/bin/sh

set -euo pipefail

ENVIRONMENT=${1:-test}  # TODO: limit to valid choices (production, test)

export GNUPGHOME="$(mktemp -d)"
cat >$GNUPGHOME/batch <<EOF
Key-Type: RSA
Key-Length: 2048
Key-Usage: sign
%no-protection
Name-Real: @psliwka's APT archive
Name-Comment: $ENVIRONMENT automatic signing key
Name-Email: apt@psliwka.info
Expire-Date: 5y
Revoker: 1:0355A06EF2482BD8F5D5656F55CBB335270DBF50
EOF
gpg --batch --generate-key $GNUPGHOME/batch
gpg --list-secret-keys 1>&2
gpg --export-secret-keys --armor
rm -r $GNUPGHOME
